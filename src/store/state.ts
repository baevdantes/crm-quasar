import IAppState from "../interfaces/IAppState";

const state: IAppState = {
  isLoadingPage: false
};

export default state;
